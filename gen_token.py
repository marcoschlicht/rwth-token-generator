#!/usr/bin/env python3

import requests
import webbrowser
import time
import logging
import json
import os
import argparse
from dotenv import load_dotenv

# OAuth Api Doku: https://oauth.campus.rwth-aachen.de/doc/
# Project Name: RWTH Token Generator
# alias rwthtoken='path/gen_rwth_token.py | xclip -selection clipboard'


def print_all(access_token, expires_in, print_time):
    if print_time:
        print(access_token)
        print("Token expires in {}h and {}min".format(expires_in // 3600, (expires_in % 3600) // 60))
    else:
        print(access_token, end="")


def new_code(client_id, scope, code_url):
    # make request
    data = {
        "client_id": client_id,
        "scope": scope
    }
    response = requests.post(code_url, data=data).json()
    logging.debug(response)

    if response["status"].startswith("error"):
        exit()

    return response


def verify_user(client_id, verification_url, user_code):
    # append to verification_url
    verification_url += "?q=verify&d=" + user_code
    # open browser
    webbrowser.open(verification_url)


def wait_until_user_is_verified(client_id, device_code, wait, token_url):
    data = {
        "client_id": client_id,
        "code": device_code,
        "grant_type": "device"
    }

    # wait for user to enter code
    # repeat request until authorized
    while True:
        response = requests.post(token_url, data=data).json()
        logging.debug(response)
        if response["status"] != "error: authorization pending.":
            break
        else:
            logging.info("waiting {} seconds".format(wait))
            time.sleep(wait)
            continue

    return response


def tokeninfo(client_id, tokeninfo_url, access_token):
    data = {
        "client_id": client_id,
        "access_token": access_token
    }
    response = requests.post(tokeninfo_url, data=data).json()
    logging.debug(response)

    if response["state"] == "valid":
        return response
    else:
        return None


def refresh_token(client_id, refresh_token, token_url):
    data = {
        "client_id": client_id,
        "refresh_token": refresh_token,
        "grant_type": "refresh_token"
    }
    response = requests.post(token_url, data=data).json()
    logging.debug(response)

    return response


def save_token(filename, access_token, refresh_token):
    file_content = {
        "refresh_token": refresh_token,
        "access_token": access_token
    }
    # convert to json and write to file
    with open(filename, "w") as f:
        json.dump(file_content, f)


def new_session(client_id, scope, code_url, token_url, filename, print_time):
    # new session
    code_response = new_code(client_id, scope, code_url)

    verify_user(client_id, code_response["verification_url"], code_response["user_code"])
    login_response = wait_until_user_is_verified(client_id, code_response["device_code"], code_response["interval"], token_url)

    save_token(filename, login_response["access_token"], login_response["refresh_token"])
    print_all(login_response["access_token"], login_response["expires_in"], print_time)


def old_session(client_id, access_token, refresh_token, token_url, tokeninfo_url, print_time):
    # check if token is valid
    info = tokeninfo(client_id, tokeninfo_url, access_token)
    if info is not None:
        # token is valid
        print_all(access_token, info["expires_in"], print_time)
    else:
        # refresh token
        refresh_response = refresh_token(client_id, refresh_token, token_url)
        save_token(filename, refresh_response["access_token"], refresh_token)
        print_all(refresh_response["access_token"], refresh_response["expires_in"], print_time)


def open_file(filename):
    try:
        with open(filename, "r") as f:
            file_content = json.load(f)
            access_token = file_content["access_token"]
            refresh_token = file_content["refresh_token"]
    except (FileNotFoundError, KeyError, json.decoder.JSONDecodeError):
        return None
    return access_token, refresh_token


# command line arguments
#   -t: token and time
#   --debug: debug infos
#   --help: help
def parse_args():
    parser = argparse.ArgumentParser(description="Get access token for RWTH services")
    parser.add_argument("-t", "--token-time", action="store_true", help="print token and time")
    parser.add_argument("--debug", action="store_true", help="print debug infos")
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    return args


def main():
    # parse command line arguments
    args = parse_args()

    # load from .env file
    load_dotenv()
    client_id = os.getenv("CLIENT_ID")
    filename = os.getenv("FILENAME")

    # constants
    scope = "moodle.rwth campus.rwth userinfo.rwth ub.rwth"
    code_url = "https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/code"
    token_url = "https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/token"
    tokeninfo_url = "https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/tokeninfo"

    file = open_file(filename)
    if file is not None:
        access_token, refresh_token = file
        old_session(client_id, access_token, refresh_token, token_url, tokeninfo_url, args.token_time)
    else:
        new_session(client_id, scope, code_url, token_url, filename, args.token_time)


if __name__ == '__main__':
    main()
