# README

## Installation

First you need to get a client id from RWTH Aachen University. You can get one by writing an email to the Service Desk.

1. Install python3
2. Install pip3
3. `pip3 install python-dotenv requests`
4. Create .env file in same directory as script
5. Add `CLIENT_ID` and `FILENAME` to .env file
6. Run script

## Usage
1. Run script
2. Open browser and login
3. Copy token from terminal
4. Paste token into application

If token is expired, run script again, it will not open browser again.

## .env file
```
CLIENT_ID=...
FILENAME=token.json
```

## Help
```
usage: gen_token.py [-h] [-o] [-t] [--debug]

Get access token for RWTH services

optional arguments:
  -h, --help            show this help message and exit
  -t, --token-time      print token and time
  --debug               print debug infos
```

## Example
```
$ python3 gen_token.py
abcdefghijklmnopqrstuvwxyz0123456789abcdef
```

## Example with time
```
$ python3 gen_token.py -t
abcdefghijklmnopqrstuvwxyz0123456789abcdef
Token expires in 1h and 21m.
```

